import { TestBed, inject } from '@angular/core/testing';

import { RouteInterceptService } from './route-intercept.service';

describe('RouteInterceptService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouteInterceptService]
    });
  });

  it('should be created', inject([RouteInterceptService], (service: RouteInterceptService) => {
    expect(service).toBeTruthy();
  }));
});

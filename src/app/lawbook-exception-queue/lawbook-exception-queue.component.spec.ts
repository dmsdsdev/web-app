import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawbookExceptionQueueComponent } from './lawbook-exception-queue.component';

describe('LawbookExceptionQueueComponent', () => {
  let component: LawbookExceptionQueueComponent;
  let fixture: ComponentFixture<LawbookExceptionQueueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawbookExceptionQueueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawbookExceptionQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

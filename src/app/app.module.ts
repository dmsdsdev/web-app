import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ChipsModule} from 'primeng/primeng';
import {ButtonModule} from 'primeng/primeng';
import {MenuModule,MenuItem} from 'primeng/primeng';
import {PanelModule} from 'primeng/primeng';
import {TabViewModule} from 'primeng/primeng';
import {SidebarModule} from 'primeng/primeng';
import {ContextMenuModule} from 'primeng/primeng';
import {DataTableModule,SharedModule} from 'primeng/primeng';
import {ChartModule} from 'primeng/primeng';
import {FileUploadModule} from 'primeng/primeng';
import {TieredMenuModule} from 'primeng/primeng';
import {InputSwitchModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';
import {DropdownModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import { ZoomableCanvasComponent } from '@durwella/zoomable-canvas';


import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { routing } from './app.routing';
import { TopbarComponent } from './topbar/topbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { BatchCreationComponent } from './batch-creation/batch-creation.component';
import { DataEntryComponent } from './data-entry/data-entry.component';
import { SharedService } from './shared.service';
import { RouteInterceptService } from './route-intercept.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ToolbarModule } from 'primeng/components/toolbar/toolbar';
import { ProgressBarModule } from 'primeng/components/progressbar/progressbar';
import { TestComponent } from './test/test.component';
import { GazetteCreationComponent } from './gazette-creation/gazette-creation.component';
import { LawbookCreationComponent } from './lawbook-creation/lawbook-creation.component';
import { GazetteDataEntryComponent } from './gazette-data-entry/gazette-data-entry.component';
import { LawbookDataEntryComponent } from './lawbook-data-entry/lawbook-data-entry.component';
import { ExceptionQueueComponent } from "./exception-queue/exception-queue.component";
import { GazetteExceptionQueueComponent } from './gazette-exception-queue/gazette-exception-queue.component';
import { LawbookExceptionQueueComponent } from './lawbook-exception-queue/lawbook-exception-queue.component';
import { VerificationEntryComponent } from './verification-entry/verification-entry.component';
import { GazetteVerificationEntryComponent } from './gazette-verification-entry/gazette-verification-entry.component';
import { LawbookVerificationEntryComponent } from './lawbook-verification-entry/lawbook-verification-entry.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    BatchCreationComponent,
    TopbarComponent,
    SidebarComponent,
    FooterComponent,
    ChangePasswordComponent,
    DataEntryComponent,
    DashboardComponent,
    TestComponent,
    GazetteCreationComponent,
    LawbookCreationComponent,
    GazetteDataEntryComponent,
    ZoomableCanvasComponent,
    LawbookDataEntryComponent,
    ExceptionQueueComponent,
    GazetteExceptionQueueComponent,
    LawbookExceptionQueueComponent,
    VerificationEntryComponent,
    GazetteVerificationEntryComponent,
    LawbookVerificationEntryComponent, 
   
    
  ],
  imports: [
    // BrowserModule,
    routing,
    BrowserAnimationsModule,
    SidebarModule,
    DataTableModule,
    SharedModule,
    InputSwitchModule,
    ToolbarModule,
    ChipsModule,
    ContextMenuModule,
    ProgressBarModule,
    FormsModule,
    ButtonModule,
    MenuModule,
    PanelModule,
    TabViewModule,
    ChartModule,
    FileUploadModule,
    TieredMenuModule,
    InputTextModule,
    DropdownModule,
    DialogModule
   
  
    
  ],
  providers: [
    SharedService,
    RouteInterceptService
  ],
  bootstrap: [HomeComponent]
})
export class AppModule { }

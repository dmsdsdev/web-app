import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable()
export class SharedService {

  public singleDataArray = new Map<Object, Object>();

  private source = new BehaviorSubject<Map<Object, Object>>(this.singleDataArray);;

  public currentState;

  constructor() {
    
    this.currentState = this.source.asObservable();
  
    
  }

  public parseObject(key: string, value: Object) {
    
    this.singleDataArray.set(key, value);

    this.source.next(this.singleDataArray);
  }


}

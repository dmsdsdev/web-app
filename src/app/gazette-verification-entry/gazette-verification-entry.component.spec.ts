import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GazetteVerificationEntryComponent } from './gazette-verification-entry.component';

describe('GazetteVerificationEntryComponent', () => {
  let component: GazetteVerificationEntryComponent;
  let fixture: ComponentFixture<GazetteVerificationEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GazetteVerificationEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GazetteVerificationEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

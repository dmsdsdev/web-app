import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-gazette-data-entry',
  templateUrl: './gazette-data-entry.component.html',
  styleUrls: ['./gazette-data-entry.component.css']
})



export class GazetteDataEntryComponent implements OnInit {

  cities1: SelectItem[];
  cities2: SelectItem[];

  constructor() { 
    this.cities1 = [
      {label:'Select Language', value:null},
      {label:'Sinhala', value:{id:1, name: 'Sinhala', code: 'S'}},
      {label:'Tamil', value:{id:2, name: 'Tamil', code: 'T'}},
      {label:'English', value:{id:3, name: 'English', code: 'E'}}
     
    ];

    this.cities2 = [
      {label:'Select Option', value:null},
      {label:'Yes', value:{id:1, name: 'Yes', code: 'Y'}},
      {label:'No', value:{id:2, name: 'No', code: 'N'}}
     
     
    ];
  }

  ngOnInit() {
  }

}

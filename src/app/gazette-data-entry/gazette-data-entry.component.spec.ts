import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GazetteDataEntryComponent } from './gazette-data-entry.component';

describe('GazetteDataEntryComponent', () => {
  let component: GazetteDataEntryComponent;
  let fixture: ComponentFixture<GazetteDataEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GazetteDataEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GazetteDataEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  displayMenu: boolean;
  progressBar : boolean;

  constructor(public sharedService : SharedService) { 

  }

  ngOnInit() { 

    let me = this;
    
        this.displayMenu = false;
     
         this.sharedService.currentState.subscribe(data=>{
           
          this.displayMenu = Boolean ((data.get("menu")==null)? false : data.get("menu") ) ;
         
        });

        this.sharedService.currentState.subscribe(data=>{

          this.progressBar = Boolean ( (data.get("progressbar") == null)? false : data.get("progressbar") );

        });  
    
   
    
    
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawbookVerificationEntryComponent } from './lawbook-verification-entry.component';

describe('LawbookVerificationEntryComponent', () => {
  let component: LawbookVerificationEntryComponent;
  let fixture: ComponentFixture<LawbookVerificationEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawbookVerificationEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawbookVerificationEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

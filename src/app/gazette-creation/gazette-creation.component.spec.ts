import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GazetteCreationComponent } from './gazette-creation.component';

describe('GazetteCreationComponent', () => {
  let component: GazetteCreationComponent;
  let fixture: ComponentFixture<GazetteCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GazetteCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GazetteCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

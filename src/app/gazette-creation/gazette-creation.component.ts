import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from "../shared.service";

@Component({
  selector: 'app-gazette-creation',
  templateUrl: './gazette-creation.component.html',
  styleUrls: ['./gazette-creation.component.css']
})
export class GazetteCreationComponent implements OnInit {

  constructor(public sharedService : SharedService, public router: Router) {

   }

  ngOnInit() {


  }

  doLogin(){
    this.sharedService.parseObject("progressbar",true);
  }

  doLogin2(){
    this.sharedService.parseObject("progressbar",false);
  }

  doBatchCreation() {
    this.router.navigateByUrl("/batch-creation");
    console.log("do batch creation method");
  }

}

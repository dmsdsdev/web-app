import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  showUserIcon: boolean;

  constructor(public router: Router, public sharedService :SharedService) {
    console.log("top bar init")
   }

  ngOnInit() {

    this.showUserIcon = true;
    
  }

  menuShow(){

    this.sharedService.parseObject('menu',true);
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-verification-entry',
  templateUrl: './verification-entry.component.html',
  styleUrls: ['./verification-entry.component.css']
})
export class VerificationEntryComponent implements OnInit {

  constructor(private sharedService : SharedService,private router:Router) { }

  ngOnInit() {
  }

}

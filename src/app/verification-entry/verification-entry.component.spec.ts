import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationEntryComponent } from './verification-entry.component';

describe('VerificationEntryComponent', () => {
  let component: VerificationEntryComponent;
  let fixture: ComponentFixture<VerificationEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificationEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GazetteExceptionQueueComponent } from './gazette-exception-queue.component';

describe('GazetteExceptionQueueComponent', () => {
  let component: GazetteExceptionQueueComponent;
  let fixture: ComponentFixture<GazetteExceptionQueueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GazetteExceptionQueueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GazetteExceptionQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

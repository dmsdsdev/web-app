import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lawbook-creation',
  templateUrl: './lawbook-creation.component.html',
  styleUrls: ['./lawbook-creation.component.css']
})
export class LawbookCreationComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  doBatchCreation() {
    this.router.navigateByUrl("/batch-creation");
    console.log("do batch creation method");
  }
}

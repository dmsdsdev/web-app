import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawbookCreationComponent } from './lawbook-creation.component';

describe('LawbookCreationComponent', () => {
  let component: LawbookCreationComponent;
  let fixture: ComponentFixture<LawbookCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawbookCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawbookCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

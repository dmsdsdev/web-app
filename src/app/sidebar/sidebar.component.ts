import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/primeng';
import { RouteInterceptService } from '../route-intercept.service';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  profileMenu: { label: string; icon: string;command : any; }[];
  batchMenu: { label: string; icon: string; items:any[]; }[];
  @ViewChild("profileMenuEl") profileMenuEl;
  @ViewChild("batchMenuE1") batchMenuEl;
  

  constructor(private routerService: RouteInterceptService,private sharedService : SharedService) { 

    console.log("side bar init")
    
  }


  items: MenuItem[];

  ngOnInit() {

    let me = this;
    // this.batchMenu = [
    //   {
    //     label: 'Edit',
    //     icon: 'fa-edit',
    //     items: [
    //         {label: 'Undo', icon: 'fa-mail-forward'},
    //         {label: 'Redo', icon: 'fa-mail-reply'}
    //     ]
    //   }
    // ];


    this.profileMenu = [
      {
        label: 'Change Password',
        icon: '',
        command : function($event){
          console.log("change pass ");
          me.routerService.validateRoute("/change-password");

        }

        
      },
      {
        label: 'Signout',
        icon: '',
        command : function($event){
          me.routerService.validateRoute("/login");
        }
      }
    ];

    

  }

  openProfileMenu() {
    this.profileMenuEl.show();
  }

  tgl(){

    this.sharedService.parseObject('menu',false);
  } 
  

}





import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  data: any;
  
      constructor() {
          this.data = {
              labels: ['Batch Open','Assigned to Data Entry','Completed','Completed with errors','Assigned to Verify User','Verified','PDF Generated', 'Exported'],
              
              datasets: [
                  {
                      data: [350, 50, 100, 20, 32, 12, 52, 32],
                      backgroundColor: [
                        "#008000", 
                        "#FF6384",
                          "#36A2EB",
                          "#922B21",
                          "#EE82EE",
                          "#F39C12",
                          "#73C6B6",
                          "#F4D03F"
                      ],
                      hoverBackgroundColor: [
                        "#008000",   
                        "#FF6384",
                          "#36A2EB",
                          "#922B21",
                          "#EE82EE",
                          "#F39C12",
                          "#73C6B6",
                          "#F4D03F"
                      ]
                  }]    
              };
      }

  ngOnInit() {
  }

}

import { LoginComponent } from "./login/login.component";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { ChangePasswordComponent } from "./change-password/change-password.component";
import { BatchCreationComponent } from "./batch-creation/batch-creation.component";
import { DataEntryComponent } from "./data-entry/data-entry.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { TestComponent } from "./test/test.component";
import { GazetteCreationComponent } from "./gazette-creation/gazette-creation.component";
import { LawbookCreationComponent } from "./lawbook-creation/lawbook-creation.component";
import { GazetteDataEntryComponent } from "./gazette-data-entry/gazette-data-entry.component";
import { LawbookDataEntryComponent } from "./lawbook-data-entry/lawbook-data-entry.component";
import { ExceptionQueueComponent } from "./exception-queue/exception-queue.component";
import { GazetteExceptionQueueComponent } from "./gazette-exception-queue/gazette-exception-queue.component";
import { LawbookExceptionQueueComponent } from "./lawbook-exception-queue/lawbook-exception-queue.component";
import { VerificationEntryComponent } from "./verification-entry/verification-entry.component";
import { GazetteVerificationEntryComponent } from "./gazette-verification-entry/gazette-verification-entry.component";
import { LawbookVerificationEntryComponent } from "./lawbook-verification-entry/lawbook-verification-entry.component";


const appRoutes: Routes = [

    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: '',
        component: DashboardComponent,
        
    },
    {
        path: 'change-password',
        component: ChangePasswordComponent
    },
    
    {
        path: 'batch-creation',
        component : BatchCreationComponent
    },
    {
        path:'data-entry',
        component:DataEntryComponent
    },
    {
        path:'exception-queue',
        component:ExceptionQueueComponent
    },
    {
        path:'test',
        component:TestComponent
    },
    {
        path:'gazette-creation',
        component:GazetteCreationComponent
    },
    {
        path:'law-book-creation',
        component:LawbookCreationComponent
    },
    {
        path:'gazette-data-entry',
        component:GazetteDataEntryComponent
    },
    {
        path:'lawbook-data-entry',
        component:LawbookDataEntryComponent
    },
    
    {
        path:'exception-queue',
        component:ExceptionQueueComponent
    },
    
    {
        path:'gazette-exception-queue',
        component:GazetteExceptionQueueComponent
    },

    {
        path:'lawbook-exception-queue',
        component:LawbookExceptionQueueComponent
    },
    {
        path:'verification-entry',
        component:VerificationEntryComponent
    },
    {
        path:'gazette-verification-entry',
        component:GazetteVerificationEntryComponent
    },
    {
        path:'lawbook-verification-entry',
        component:LawbookVerificationEntryComponent
    }
    // { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
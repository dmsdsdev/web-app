import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-batch-creation',
  templateUrl: './batch-creation.component.html',
  styleUrls: ['./batch-creation.component.css']
})
export class BatchCreationComponent implements OnInit {
  cars: any[];
  
  public batchMode;

  public display;
  
  constructor(private sharedService : SharedService,private router:Router) { 
    
  }

  ngOnInit() { 
    
    this.cars = []
    
  }

  getBatchMode(){
    // console.log(">>>>>>>>>>>>",this.batchMode);

    
  }

  createMode(a){

    //console.log("BATCH MODE",this.batchMode)
    if(this.batchMode == true){
      this.router.navigate(["/gazette-creation"]);
    }else{
      this.router.navigate(["/law-book-creation"]);
    }
  }


}





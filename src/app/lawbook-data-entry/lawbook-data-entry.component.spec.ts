import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawbookDataEntryComponent } from './lawbook-data-entry.component';

describe('LawbookDataEntryComponent', () => {
  let component: LawbookDataEntryComponent;
  let fixture: ComponentFixture<LawbookDataEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawbookDataEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawbookDataEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-exception-queue',
  templateUrl: './exception-queue.component.html',
  styleUrls: ['./exception-queue.component.css']
})
export class ExceptionQueueComponent implements OnInit {
 
  constructor(private sharedService : SharedService,private router:Router) { }

  ngOnInit() {
    
  }

}
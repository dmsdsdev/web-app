import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceptionQueueComponent } from './exception-queue.component';

describe('ExceptionQueueComponent', () => {
  let component: ExceptionQueueComponent;
  let fixture: ComponentFixture<ExceptionQueueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceptionQueueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceptionQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from './shared.service';

@Injectable()
export class RouteInterceptService {

  constructor(private router: Router, private sharedService : SharedService) { 

  }

  public validateRoute(path : string){
    this.sharedService.parseObject("menu",false);
    this.router.navigateByUrl(path);
  }

}
